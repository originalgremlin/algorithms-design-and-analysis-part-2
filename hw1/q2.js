#!/usr/bin/env node

var fs = require('fs');

// import file
var file = fs.readFileSync('jobs.txt', 'utf8').split('\n');

// load file into array
var jobs = new Array(parseFloat(file[0]));
for (var i = 0, length = file.length - 2; i < length; i++) {
    jobs[i] = file[i + 1].split(' ').map(parseFloat);
}

// sort by weight - length, descending
jobs.sort(function (a, b) {
    var aScore = a[0] / a[1],
        bScore = b[0] / b[1];
    if (aScore < bScore)
        return 1;
    else if (aScore > bScore)
        return -1;
    else
        return b[0] - a[0];
});

// compute total
var completion = 0,
    total = 0;
jobs.forEach(function (job) {
    completion += job[1];
    total += completion * job[0];
});
console.log(total);
