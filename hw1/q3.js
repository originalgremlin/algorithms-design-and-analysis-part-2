#!/usr/bin/env node

// import file
var fs = require('fs'),
    file = fs.readFileSync('edges.txt', 'utf8').split('\n');

// load file into array
var counts = file[0].split(/ /).map(parseFloat),
    numVertices = counts[0],
    numEdges = counts[1],
    cost = 0;

var V = new Array(numVertices + 1);
for (var i = 0, len = V.length; i < len; i++)
    V[i] = false;

var E = new Array(numEdges);
for (var i = 0, len = E.length; i < len; i++)
    E[i] = file[i + 1].split(' ').map(parseFloat);

// initialize X = {s}
// (s is a member of V chosen arbitrarily) (X is a set of vertices)
V[1] = true;
numVertices--;

while (numVertices > 0) {
    // find the cheapest edge of G with u member of X, v not member of X
    var min = [null, null, Number.POSITIVE_INFINITY];
    E.forEach(function (e) {
        if ((V[e[0]] !== V[e[1]]) && (e[2] < min[2]))
            min = e;
    });
    // add v to X
    V[min[0]] = true;
    V[min[1]] = true;
    // update MST cost
    cost += min[2];
    numVertices--;
}

console.log(cost);
